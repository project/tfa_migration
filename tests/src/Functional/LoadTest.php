<?php

declare(strict_types=1);

namespace Drupal\Tests\tfa_migration\Functional;

use Drupal\Tests\system\Functional\Module\ModuleTestBase;

/**
 * Test TFA Migration Module.
 *
 * @group tfa_migration
 */
final class LoadTest extends ModuleTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'tfa_migration',
    'block',
    'views',
    'dblog',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->drupalPlaceBlock('page_title_block');
    $this->drupalPlaceBlock('user_login_block');
    $this->drupalLogout();
  }

  /**
   * Tests Homepage after enabling TFA Migration Module.
   */
  public function testHomepage(): void {

    // Test homepage.
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);

    // Minimal homepage title.
    $this->assertSession()->pageTextContains('Log in');

    // Assert that tables are created successfully after module installed.
    $this->assertModuleTablesExist('tfa_migration');
  }

  /**
   * Tests the TFA Migration Module unistall.
   */
  public function testModuleUninstall(): void {

    // Log In as Admin.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('/admin/modules/uninstall');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('TFA Migration Module');
    $this->submitForm(['uninstall[tfa_migration]' => TRUE], 'Uninstall');
    $this->submitForm([], 'Uninstall');
    $this->assertSession()->pageTextContains('The selected modules have been uninstalled.');
    $this->assertSession()->pageTextNotContains('TFA Migration Module');

    // Assert that tables are deleted successfully after module uninstalled.
    $this->assertModuleTablesDoNotExist('tfa_migration');

    // Visit the frontpage.
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests TFA Migration Module reinstalling after being uninstalled.
   */
  public function testReinstallAfterUninstall(): void {

    // Log In as Admin.
    $this->drupalLogin($this->adminUser);

    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Uninstall the TFA Migration Module.
    $this->container->get('module_installer')->uninstall(['tfa_migration'], FALSE);
    // Assert that tables are created successfully after module installed.
    $this->assertModuleTablesDoNotExist('tfa_migration');

    $this->drupalGet('/admin/modules');
    $page->checkField('modules[tfa_migration][enable]');
    $page->pressButton('Install');
    $assert_session->pageTextNotContains('Unable to install TFA Migration Module');
    $assert_session->pageTextContains('Module TFA Migration has been installed');
    // Assert that tables are deleted successfully after module uninstalled.
    $this->assertModuleTablesExist('tfa_migration');

    // Visit the frontpage.
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);

  }

}
