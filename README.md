## INTRODUCTION

The TFA Migration module is used to migrate TFA data from
Drupal 7 to Drupal 9 and Above.

## REQUIREMENTS

- [TFA Module](https://www.drupal.org/project/tfa)

- [Encrypt Module](https://www.drupal.org/project/encrypt)

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION

To Configure Drupal 7 TFA Private Key navigate to
`/admin/config/system/tfa-migration-settings` and enter key and save data.

You can define the private key in settings.php by adding the below
config to settings.php in your Drupal 9+ site.

`$config['tfa_migration.settings']['drupal7_private_key'] =
getenv('DRUPAL7_PRIVATE_KEY');`

To get Private Key from Drupal 7 use the below command

`drush vget drupal_private_key`

If Drush is not installed on your site use the guide from
https://drupal.stackexchange.com/questions/279004 to get the required key.

You can also run the below SQL Query in your Drupal 7 Database to get the
required key

``` SQL
SELECT name, value FROM variable WHERE name = "drupal_private_key"
```

## MAINTAINERS

Current maintainers for Drupal 9 & 10:

- Bhanu (Bhanu951) - https://www.drupal.org/u/bhanu951

Related Issue: [#3328128: Migration from Drupal 7 to Drupal 9](https://www.drupal.org/project/tfa/issues/3328128)

## MIGRATION COMMANDS

``` bash

drush ms --tag=tfa_migration

drush mim tfa_migration_totp_seed

drush mim tfa_migration_user_settings

```
