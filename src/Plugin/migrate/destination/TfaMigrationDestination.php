<?php

declare(strict_types=1);

namespace Drupal\tfa_migration\Plugin\migrate\destination;

use Drupal\Core\Database\Connection;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Plugin\migrate\destination\DestinationBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The 'tfa_migration_destination' destination plugin.
 *
 * @MigrateDestination(
 *   id = "tfa_migration_destination"
 * )
 */
final class TfaMigrationDestination extends DestinationBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs the plugin instance.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, private readonly Connection $database) {
    $this->supportsRollback = TRUE;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, ?MigrationInterface $migration = NULL): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('database'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getIds(): array {

    $ids = [];
    $ids['uid']['type'] = 'integer';
    $ids['module']['type'] = 'string';
    $ids['name']['type'] = 'string';

    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function fields(?MigrationInterface $migration = NULL): array {

    return [
      'uid' => $this->t('The row ID.'),
      'module' => $this->t('The Module Name.'),
      'name' => $this->t('The name of the settings.'),
      'value' => $this->t('The Data of the settings.'),
      'serialized' => $this->t('Serialized status of the settings.'),
    ];

  }

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []): array|bool {

    $uid = $row->getDestinationProperty('uid');
    $module = $row->getDestinationProperty('module');
    $name = $row->getDestinationProperty('name');
    $value = $row->getDestinationProperty('value');
    $serialized = $row->getDestinationProperty('serialized');

    $this->database->merge('users_data')
      ->keys([
        'uid' => $uid,
        'module' => $module,
        'name' => $name,
      ])
      ->fields([
        'value' => $value,
        'serialized' => $serialized,
      ])->execute();

    return [$uid, $module, $name];

  }

  /**
   * {@inheritdoc}
   */
  public function rollback(array $destination_identifier): void {

    $this->database->delete('users_data')
      ->condition('uid', $destination_identifier['uid'])
      ->condition('module', $destination_identifier['module'])
      ->condition('name', $destination_identifier['name'])
      ->execute();

  }

}
