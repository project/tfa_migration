<?php

declare(strict_types=1);

namespace Drupal\tfa_migration\Plugin\migrate\source;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;

/**
 * The 'migrate_tfa_user_settings' source plugin.
 *
 * @MigrateSource(
 *   id = "migrate_tfa_user_settings",
 *   source_module = "tfa_migration",
 * )
 */
final class MigrateTfaUserSettings extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query(): SelectInterface {

    $fields = [
      'uid',
      'saved',
      'status',
      'data',
    ];

    return $this->select('tfa_user_settings', 'user_settings')
      ->fields('user_settings', $fields);

  }

  /**
   * {@inheritdoc}
   */
  public function fields() {

    $fields = [
      'uid' => $this->t('The UID of the User int(10) UN PK'),
      'saved' => $this->t('The Timestamp of the settings saved int(11)'),
      'status' => $this->t('The Status of the Settings int(11)'),
      'data' => $this->t('The Data of the settings varchar(255)'),
      'module' => $this->t('The Module Name varchar(255)'),
      'name' => $this->t('The Name of the data varchar(255)'),
      'value' => $this->t('The value of the settings varchar(255)'),
      'serialized' => $this->t('Serialized status of the settings int(2)'),
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds(): array {

    $ids = [];
    $ids['uid']['type'] = 'integer';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row): bool {

    $saved = $row->getSourceProperty('saved');
    $status = $row->getSourceProperty('status');
    $data = $row->getSourceProperty('data');
    $row->setSourceProperty('module', 'tfa');
    $row->setSourceProperty('name', 'tfa_user_settings');
    $row->setSourceProperty('serialized', 1);
    // Get the Serailized User Settings Data.
    $serialized_user_settings = $this->getSerializedTfaUserSettings($saved, $status, $data);
    $row->setSourceProperty('value', $serialized_user_settings);
    return parent::prepareRow($row);

  }

  /**
   * Method to get Tfa User Settings.
   */
  protected function getSerializedTfaUserSettings($saved, $status, $data): string {

    $user_settings = [];

    // Decode the User Settings Data.
    $decoded_data = json_decode($data);

    // Instantiate $data_plugins.
    $data_plugins = [];

    // Check if the user has SMS enabled.
    if (isset($decoded_data->sms)) {
      $data_sms = boolval($decoded_data->sms);
    }
    else {
      $data_sms = FALSE;
    }

    // Reset the user's 'validation skipped' value.
    $validation_skipped = 0;

    // If the user has TFA enabled, convert the enabled plugins from their
    // Drupal 7 names and values to their Drupal 9 names and values.
    if (isset($decoded_data->plugins)) {
      // Check to see if the user has TOTP enabled. If so, add it to the
      // plugins array.
      if ($decoded_data->plugins[0] == 'tfa_basic_totp') {
        $data_plugins['tfa_totp'] = 'tfa_totp';
      }
    }

    // Check to see if the user has recovery codes enabled. If so, add it
    // to the plugins array.
    if (isset($decoded_data->plugins[1]) && ($decoded_data->plugins[1] == 'tfa_basic_recovery_code')) {
      $data_plugins['tfa_recovery_code'] = 'tfa_recovery_code';
    }

    // Set up the user settings array using the previously gathered values.
    $user_settings = [
      'saved' => intval($saved),
      'status' => intval($status),
      'data' => [
        'plugins' => $data_plugins,
        'sms' => $data_sms,
      ],
      'validation_skipped' => $validation_skipped,
    ];
    $serialize_user_settings = serialize($user_settings);

    return $serialize_user_settings;

  }

}
