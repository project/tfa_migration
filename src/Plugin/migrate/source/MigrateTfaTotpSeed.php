<?php

declare(strict_types=1);

namespace Drupal\tfa_migration\Plugin\migrate\source;

use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\State\StateInterface;
use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Drupal\tfa_migration\Service\TfaMigrationServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The 'migrate_tfa_totp_seed' source plugin.
 *
 * @MigrateSource(
 *   id = "migrate_tfa_totp_seed",
 *   source_module = "tfa_migration",
 * )
 */
final class MigrateTfaTotpSeed extends SqlBase {

  /**
   * The TFA Migration Service.
   *
   * @var \Drupal\tfa_migration\Service\TfaMigrationServiceInterface
   */
  protected TfaMigrationServiceInterface $tfaMigrationService;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, StateInterface $state, TfaMigrationServiceInterface $tfa_migration_service) {
    $this->tfaMigrationService = $tfa_migration_service;
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration, $state);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, ?MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('state'),
      $container->get('tfa_migration.service'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query(): SelectInterface {
    return $this->select('tfa_totp_seed', 'totp_seed')
      ->fields('totp_seed', ['uid', 'seed', 'created']);
  }

  /**
   * {@inheritdoc}
   */
  public function fields(): array {
    return [
      'uid' => $this->t('The UID of the User int(10) UN PK'),
      'seed' => $this->t('The TFA TOTP Seed varchar(255)'),
      'created' => $this->t('The Timestamp of the user Creation int(11)'),
      'module' => $this->t('The Module Name varchar(255)'),
      'name' => $this->t('The Name of the data varchar(255)'),
      'value' => $this->t('The value of the settings varchar(255)'),
      'serialized' => $this->t('Serialized status of the settings int(2)'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds(): array {

    $ids = [];
    $ids['uid'] = [
      'type' => 'integer',
    ];
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row): bool {

    // Encrypted Seed Data.
    $seed = $row->getSourceProperty('seed');
    // Timestamp of the Seed Created.
    $created = $row->getSourceProperty('created');

    // Note: The TOTP seed must be decrypted and re-encrypted because the
    // encryption method changed between Drupal 7 and Drupal 9. This will
    // ensure TFA authentication will work in Drupal 9.
    // Decrypt the TOTP seed.
    $decrypted_text = $this->tfaMigrationService->decryptData($seed);

    // Encrypt the TOTP seed.
    $encrypted_text = $this->tfaMigrationService->encryptTotpSeed($decrypted_text);

    $totp_seed = [
      'seed' => $encrypted_text,
      'created' => $created,
    ];

    $serialize_totp_seed = serialize($totp_seed);

    $row->setSourceProperty('decrypted_seed', $decrypted_text);
    $row->setSourceProperty('module', 'tfa');
    $row->setSourceProperty('name', 'tfa_totp_seed');
    $row->setSourceProperty('value', $serialize_totp_seed);
    $row->setSourceProperty('serialized', 1);

    return parent::prepareRow($row);

  }

}
