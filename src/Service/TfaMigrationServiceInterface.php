<?php

declare(strict_types=1);

namespace Drupal\tfa_migration\Service;

/**
 * Service Class to Migrate TFA Data.
 */
interface TfaMigrationServiceInterface {

  /**
   * Decrypts recovery codes from the Drupal 7 site.
   *
   * This function is basically a duplicate of the decrypt function in the TFA
   * module's McryptAES128Encryption class (located at tfa / src / Plugin /
   * EncryptionMethod / McryptAES128Encryption.php). The one difference is that
   * we base64_decode the data before running the rest of the function.
   *
   * Source: TFA module
   *
   * @param string $data
   *   The data to be decrypted.
   *
   * @return string|bool
   *   Returns the Decrypted Data.
   *
   * @see tfa/src/Plugin/EncryptionMethod/McryptAES128Encryption.php
   */
  public function decryptData(string $data): string|bool;

  /**
   * Encrypts TOTP seed data for the Drupal 9 site.
   *
   * This function uses the Encrypt module's encrypt function to encrypt the TFA
   * TOTP seed in the way the TFA module expects (in this case, encrypted and
   * base64 encoded).
   *
   * Source: Encrypt (contributed module)
   *
   * @param string $data
   *   The data to be encrypted.
   *
   * @return string
   *   Returns the Encrypted TOTP Seed.
   *
   * @see https://www.drupal.org/docs/contributed-modules/encrypt/general-drupal-8-encrypt-setup-and-recommendations
   */
  public function encryptTotpSeed(string $data): string;

}
