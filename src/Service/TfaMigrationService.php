<?php

declare(strict_types=1);

namespace Drupal\tfa_migration\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\encrypt\EncryptionProfileManagerInterface;
use Drupal\encrypt\EncryptServiceInterface;
use Drupal\migrate\MigrateException;

/**
 * {@inheritdoc}
 */
final class TfaMigrationService implements TfaMigrationServiceInterface {

  use StringTranslationTrait;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * Constructs a TFA Migration Service object.
   */
  public function __construct(
    private readonly ConfigFactoryInterface $configFactory,
    private readonly EncryptServiceInterface $encryption,
    private readonly LoggerChannelFactoryInterface $loggerFactory,
    private readonly MessengerInterface $messenger,
    private readonly StateInterface $state,
    private readonly Connection $connection,
    private readonly EncryptionProfileManagerInterface $encryptionProfile,
  ) {
    $this->logger = $loggerFactory->get('tfa_migration');
  }

  /**
   * {@inheritdoc}
   */
  public function decryptData(string $data): string|bool {

    // The private key value from the Drupal 7 site.
    $drupal7_private_key = $this->configFactory->get('tfa_migration.settings')->get('drupal7_private_key') ?? '';

    if (empty($drupal7_private_key)) {
      $this->logger->error($this->t("Drupal 7 Private Key not defined."));
      throw new MigrateException(sprintf('Drupal 7 Private Key not defined.'));
    }

    $data = base64_decode($data);

    $crypto_data = Json::decode($data);

    if (empty($crypto_data['version']) || empty($crypto_data['iv_base64']) || empty($crypto_data['ciphertext_base64'])) {
      // Backwards compatibility with the old Mcrypt scheme.
      return extension_loaded('mcrypt') ? $this->decryptLegacyDataWithMcrypt($data, $drupal7_private_key) : $this->decryptLegacyDataWithOpenSSL($data, $drupal7_private_key);
    }
    else {
      $iv = base64_decode($crypto_data['iv_base64']);
      $ciphertext = base64_decode($crypto_data['ciphertext_base64']);
      return openssl_decrypt($ciphertext, 'aes-256-cbc', $drupal7_private_key, (int) TRUE, $iv);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function encryptTotpSeed($data): string {

    $encryption_profile_id = $this->configFactory->get('tfa.settings')->get('encryption');
    $encryption_profile = $this->encryptionProfile->getEncryptionProfile($encryption_profile_id);
    $encrypted_text = base64_encode($this->encryption->encrypt($data, $encryption_profile));

    return $encrypted_text;
  }

  /**
   * Decrypt using the deprecated Mcrypt extension.
   *
   * As used by an earlier version of this module.
   *
   * @param string $text
   *   The text to be decrypted.
   * @param string $key
   *   The key to decrypt the text with.
   *
   * @return string
   *   The decrypted text
   *
   * @see https://git.drupalcode.org/project/tfa/-/blob/7.x-2.x/tfa.inc?ref_type=heads#L705
   *
   * @noinspection PhpDeprecationInspection
   */
  private function decryptLegacyDataWithMcrypt($text, $key): string {

    $this->logger->info($this->t("TFA Module is using mcrypt method"));

    $td = mcrypt_module_open('rijndael-128', '', 'cbc', '');
    $iv = substr($text, 0, mcrypt_enc_get_iv_size($td));

    $data = substr($text, mcrypt_enc_get_iv_size($td));
    $key = substr($key, 0, mcrypt_enc_get_key_size($td));

    mcrypt_generic_init($td, $key, $iv);

    $decrypted_text = mdecrypt_generic($td, $data);
    // Return only the message and none of its padding.
    [$length, $padded_data] = explode('|', $decrypted_text, 2);
    $text = substr($padded_data, 0, (int) $length);

    mcrypt_generic_deinit($td);
    mcrypt_module_close($td);

    return $text;
  }

  /**
   * Use OpenSSL to decrypt data that was originally encrypted with Mcrypt.
   *
   * As used by an earlier version of this module.
   *
   * @param string $text
   *   The text to be decrypted.
   * @param string $key
   *   The key to decrypt the text with.
   *
   * @return string|bool
   *   The decrypted text, or empty string on failure.
   *
   * @see https://git.drupalcode.org/project/tfa/-/blob/7.x-2.x/tfa.inc?ref_type=heads#L738
   *
   * phpcs:disable Drupal.NamingConventions.ValidFunctionName.ScopeNotCamelCaps
   */
  private function decryptLegacyDataWithOpenSSL($text, $key): string|bool {

    // Based on return value of mcrypt_enc_get_key_size($td).
    $key_size = 32;
    // Based on return value of mcrypt_enc_get_iv_size($td).
    $iv_size = 16;
    $key = substr($key, 0, $key_size);
    $iv = substr($text, 0, $iv_size);
    $data = substr($text, $iv_size);
    // Using 3 instead of the constant OPENSSL_NO_PADDING, for PHP 5.3.
    $decrypted_text = openssl_decrypt($data, 'aes-256-cbc', $key, 3, $iv);

    // Return only the message and none of its padding.
    if (strpos($decrypted_text, '|') !== FALSE) {
      [$length, $padded_data] = explode('|', $decrypted_text, 2);
      $decrypted_text = substr($padded_data, 0, (int) $length);
      return $decrypted_text;
    }
    else {
      return '';
    }
  }

}
