<?php

declare(strict_types=1);

namespace Drupal\tfa_migration\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure TFA Migration settings for this site.
 */
final class TfaMigrationSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'tfa_migration_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['tfa_migration.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['drupal7_private_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Drupal 7 Private Key'),
      '#required' => TRUE,
      '#default_value' => $this->config('tfa_migration.settings')->get('drupal7_private_key'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */ // phpcs:disable
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    // @todo add validation for private key size.
    parent::validateForm($form, $form_state);
  } // phpcs:enable

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('tfa_migration.settings')
      ->set('drupal7_private_key', trim($form_state->getValue('drupal7_private_key')))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
